package it.unibo.oop.lab06.generics1;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class GraphImpl<N> implements Graph<N> {

	Map<N, Set<N>> edges = new HashMap<>();
	
	@Override
	public void addNode(N node) {
		edges.putIfAbsent(node, new HashSet<>());
	}

	@Override
	public void addEdge(N source, N target) {
		if (nodesExist(source, target)) {
			edges.get(source).add(target);
		}
		
	}

	@Override
	public Set<N> nodeSet() {
		return new HashSet<>(edges.keySet());
	}
	
	//COSA VUOL DIRE FINAL QUI??
	@SafeVarargs
	private final boolean nodesExist(final N... nodes) {
		for (final N node : nodes) {
			if (edges.containsValue(node)) {
				throw new IllegalArgumentException("No such node " + node);
			}
		}
		return true;
		
	}

	@Override
	public Set<N> linkedNodes(N node) {
		return edges.get(node);
	}

	@Override
	public List<N> getPath(N source, N target) {
		// TODO Auto-generated method stub
		return null;
	}

}
