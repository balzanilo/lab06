package it.unibo.oop.lab.exception1;

public class NotEnoughBatteryException extends Exception{

	private static final long serialVersionUID = 1L;
	private double batteryLevel = 0.0;
	
	public NotEnoughBatteryException(double batteryLevel) {
		this.batteryLevel=batteryLevel;
	}
	
	public String getMessage() {
        return "Battery error: " + this.batteryLevel;
    }
}
