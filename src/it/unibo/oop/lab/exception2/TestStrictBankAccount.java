package it.unibo.oop.lab.exception2;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * JUnit test to test
 * {@link StrictBankAccount}.
 * 
 */
public class TestStrictBankAccount {

    /**
     * Used to test Exceptions on {@link StrictBankAccount}.
     */
    @Test
    public void testBankOperations() {
        /*
         * 1) Creare due StrictBankAccountImpl assegnati a due AccountHolder a
         * scelta, con ammontare iniziale pari a 10000 e nMaxATMTransactions=10
         * 
         * 2) Effetture un numero di operazioni a piacere per verificare che le
         * eccezioni create vengano lanciate soltanto quando opportuno, cioè in
         * presenza di un id utente errato, oppure al superamento del numero di
         * operazioni ATM gratuite.
         */
    	AccountHolder holder1 = new AccountHolder("Lorenzo", "Balzani", 0);
    	StrictBankAccount bank1 = new StrictBankAccount(holder1.getUserID(), 10_000, 10);
    	AccountHolder holder2 = new AccountHolder("Mario", "Rossi", 1);
    	StrictBankAccount bank2 = new StrictBankAccount(holder2.getUserID(), 10_000, 10);
    
    	
    	for (int i=0; i<11; i++) {
    		try {
    			bank1.deposit(holder1.getUserID(), 100);
    		} catch(TransactionsOverQuotaException | WrongAccountHolderException e) {
    			fail("This code did not have to generate an exception!");
    		}
    	}
    	
    	try {
    		bank2.withdrawFromATM(holder1.getUserID(), 1_000);
    		fail("Wrong UserID");
    	} catch (WrongAccountHolderException e) {
    		assertNotNull(e);
    	}
    	
    	try {
    		bank1.withdrawFromATM(holder1.getUserID(), 1_000);
    		fail("Transactions number over quota");
    	} catch (TransactionsOverQuotaException | NotEnoughFoundsException e) {
    		assertNotNull(e);
    	}
    	
    	try {
    		bank2.withdraw(holder2.getUserID(), 100_000);
    		fail("Amount to high to be withdrawn");
    	} catch (NotEnoughFoundsException e) {
    		assertNotNull(e);
    	}
    }
}
